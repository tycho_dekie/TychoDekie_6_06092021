var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _TagCloud_instances, _TagCloud_tags, _TagCloud_onFilter, _TagCloud_updateDom;
// Represents the tag cloud and fires events when tags are clicked.
//
export class TagCloud {
    constructor(tags) {
        _TagCloud_instances.add(this);
        _TagCloud_tags.set(this, void 0);
        __classPrivateFieldSet(this, _TagCloud_tags, new Map, "f");
        for (const t of tags) {
            __classPrivateFieldGet(this, _TagCloud_tags, "f").set(t, false);
        }
        this.container = document.createElement("div");
        this.container.classList.add("tags");
        // https://github.com/microsoft/TypeScript/issues/28357#issuecomment-711131035
        //
        window.addEventListener("filter-change", { handleEvent: __classPrivateFieldGet(this, _TagCloud_instances, "m", _TagCloud_onFilter).bind(this) });
    }
    html(label) {
        for (const s of __classPrivateFieldGet(this, _TagCloud_tags, "f")) {
            const t = document.createElement("span");
            t.setAttribute("aria-label", `${label}${s[0]}`);
            t.setAttribute("tabindex", "0");
            t.setAttribute("role", "checkbox");
            t.setAttribute("aria-checked", "false");
            t.classList.add("tag");
            t.classList.add("tag--" + s[0]);
            t.innerText = s[0];
            t.addEventListener("click", this.toggle.bind(this, s[0]));
            this.container.appendChild(t);
        }
        return this.container;
    }
    toggle(tag) {
        if (!__classPrivateFieldGet(this, _TagCloud_tags, "f").has(tag))
            throw "TagCloud: Unknown tag clicked";
        __classPrivateFieldGet(this, _TagCloud_tags, "f").get(tag) ?
            __classPrivateFieldGet(this, _TagCloud_tags, "f").set(tag, false)
            : __classPrivateFieldGet(this, _TagCloud_tags, "f").set(tag, true);
        const event = new CustomEvent("filter-change", { detail: new Map(__classPrivateFieldGet(this, _TagCloud_tags, "f")) });
        window.dispatchEvent(event);
    }
}
_TagCloud_tags = new WeakMap(), _TagCloud_instances = new WeakSet(), _TagCloud_onFilter = function _TagCloud_onFilter(evt) {
    __classPrivateFieldSet(this, _TagCloud_tags, evt.detail, "f");
    __classPrivateFieldGet(this, _TagCloud_instances, "m", _TagCloud_updateDom).call(this);
}, _TagCloud_updateDom = function _TagCloud_updateDom() {
    for (const [tag, checked] of __classPrivateFieldGet(this, _TagCloud_tags, "f")) {
        const span = this.container.querySelector(".tag--" + tag);
        // The tagclouds on the photographers don't have all the tags.
        //
        if (!span)
            continue;
        if (checked) {
            span.classList.add("filter__active");
            span.setAttribute("aria-checked", "true");
        }
        else {
            span.classList.remove("filter__active");
            span.setAttribute("aria-checked", "false");
        }
    }
};
