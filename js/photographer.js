var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var _Photographer_summary;
import * as util from "./util.js";
import { TagCloud } from "./tagCloud.js";
export class Photographer {
    constructor(json) {
        _Photographer_summary.set(this, null);
        this.name = json.name;
        this.id = json.id;
        this.city = json.city;
        this.country = json.country;
        this.tags = json.tags;
        this.tagline = json.tagline;
        this.price = json.price;
        this.portrait = json.portrait;
        Object.freeze(this.tags);
    }
    // Create the html for the profile summary shown on the home page.
    // It will be stored for re-use.
    //
    profileSummary() {
        if (__classPrivateFieldGet(this, _Photographer_summary, "f") !== null)
            return __classPrivateFieldGet(this, _Photographer_summary, "f");
        const html = `
		<div class="profile-summary profile-${this.id}">

			<a href="profile.html?id=${this.id}" tabindex="-1">
				<div class="portrait">
					<img class="portrait--img" src="img/id/${this.portrait}" alt="Portrait du/de la photographe ${this.name}">
					<div class="portrait--shadow"></div>
				</div>
				<h2 class="name"><a href="profile.html?id=${this.id}" aria-label="Liens vers le profile de ${this.name}">${this.name}</a></h2>
			</a>

			<p class="location">${this.city}, ${this.country}</p>
			<p class="tagline">${this.tagline}</p>
			<p class="price">${this.price}€/jour</p>

		</div>

		`;
        __classPrivateFieldSet(this, _Photographer_summary, util.toDom(html), "f");
        const cloud = new TagCloud(this.tags);
        __classPrivateFieldGet(this, _Photographer_summary, "f").appendChild(cloud.html("Filtrer les images par le mot clef: "));
        return __classPrivateFieldGet(this, _Photographer_summary, "f");
    }
    // Generate the block on top of the profile page.
    //
    profileHeading() {
        const html = `
		<div class="profile-heading">

			<div class="profile--align">

				<div class="profile--text">
					<div class="name--container">
						<h1 class="name">${this.name}</h1>
						<button id="button--contact">Contactez-moi</button>
					</div>
					<p class="location">${this.city}, ${this.country}</p>
					<p class="tagline">${this.tagline}</p>
				</div>

				<div class="portrait">
					<img class="portrait--img" src="img/id/${this.portrait}" alt="">
					<div class="portrait--shadow"></div>
				</div>

			</div>

			${this.tagList()}

		</div>

		`;
        const div = util.toDom(html);
        return div;
    }
    tagList() {
        let tags = "";
        for (const t of this.tags) {
            tags += `<a href="index.html?filter=${t}" class="tag" aria-label="Filtrer les photographes par le mot clé: ${t}">${t}</a>`;
        }
        return `<div class="tags">${tags}</div>`;
    }
}
_Photographer_summary = new WeakMap();
