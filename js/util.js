export function toDom(input) {
    const template = document.createElement("template");
    template.innerHTML = input;
    return template.content.firstElementChild;
}
export function mediaArrayEquals(a1, a2) {
    if (a1.length != a2.length)
        return false;
    let i = a1.length;
    while (i--) {
        if (a1[i].id !== a2[i].id)
            return false;
    }
    return true;
}
