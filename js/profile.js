var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var _Profile_instances, _Profile_media, _Profile_photographer, _Profile_gallery, _Profile_likesPrice, _Profile_getPhotographer;
import { Gallery } from "./gallery.js";
import { Contact } from "./contact.js";
export class Profile {
    constructor(data, router) {
        _Profile_instances.add(this);
        // The media of this photographer.
        //
        _Profile_media.set(this, void 0);
        _Profile_photographer.set(this, void 0);
        _Profile_gallery.set(this, void 0);
        __classPrivateFieldSet(this, _Profile_photographer, __classPrivateFieldGet(this, _Profile_instances, "m", _Profile_getPhotographer).call(this, data, router), "f");
        __classPrivateFieldSet(this, _Profile_media, data.mediaByAuthor(__classPrivateFieldGet(this, _Profile_photographer, "f")), "f");
        const main = document.querySelector("main");
        main.prepend(__classPrivateFieldGet(this, _Profile_photographer, "f").profileHeading());
        __classPrivateFieldSet(this, _Profile_gallery, new Gallery(__classPrivateFieldGet(this, _Profile_media, "f")), "f");
        new Contact(__classPrivateFieldGet(this, _Profile_photographer, "f").name); // functionality of contact form.
        __classPrivateFieldGet(this, _Profile_instances, "m", _Profile_likesPrice).call(this);
        document.title = `FishEye profile: ${__classPrivateFieldGet(this, _Profile_photographer, "f").name}`;
    }
}
_Profile_media = new WeakMap(), _Profile_photographer = new WeakMap(), _Profile_gallery = new WeakMap(), _Profile_instances = new WeakSet(), _Profile_likesPrice = function _Profile_likesPrice() {
    const price = document.querySelector("#likes-price > .price");
    const likes = document.querySelector("#likes-price > .likes");
    price.innerText = `${__classPrivateFieldGet(this, _Profile_photographer, "f").price}€/jour`;
    likes.prepend(__classPrivateFieldGet(this, _Profile_gallery, "f").totalLikes.toString());
}, _Profile_getPhotographer = function _Profile_getPhotographer(data, router) {
    const id = router.getQueryVar("id");
    // TODO: redirect to main page?
    //
    if (id === null)
        throw "Profile: Cannot load profile without ID.";
    const parsed = parseInt(id, 10);
    if (isNaN(parsed))
        throw "Profile: ID must be a number.";
    const p = data.findPhotographer(parsed);
    if (typeof p === "undefined")
        throw "Profile: Unknown photographer.";
    else
        return p;
};
