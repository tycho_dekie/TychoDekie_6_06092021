import * as Iface from "./deser.js";
import * as util  from "./util.js";



export abstract class Media
{
	id            : number                  ;
	photographerID: number                  ;
	title         : string                  ;
	tags          : string[]                ;
	likes         : number                  ;
	date          : string                  ;
	price         : number                  ;
	image?        : string                  ; // We don't really use these, but needed for deserialization of popstate.
	video?        : string                  ;
	#thumbnail    : HTMLElement | undefined ;
	#liked        = false                   ;



	// Factory
	//
	static createMedia( json: Iface.Media ) : Media
	{
		if( typeof json.image === "string" )
		{
			return new Image( json );
		}

		else if( typeof json.video === "string" )
		{
			return new Video( json );
		}

		else
		{
			throw "Invalid json data, no image, nor video"
		}
	}



	constructor( json: Iface.Media )
	{
		this.id             = json.id             ;
		this.photographerID = json.photographerID ;
		this.title          = json.title          ;
		this.tags           = json.tags           ;
		this.likes          = json.likes          ;
		this.date           = json.date           ;
		this.price          = json.price          ;
		this.image          = json.image          ;
		this.video          = json.video          ;
	}



	// The img or video tag for this media.
	//
	abstract element() : HTMLElement



	// The dimensions of the media.
	//
	abstract naturalWidth () : number
	abstract naturalHeight() : number



	// return a clone of the img/video element suitable for showing in the carousel.
	//
	abstract carousel() : HTMLElement



	// Html to go into the gallery as thumbnail. Note that this.element() is part of this.
	//
	thumbnail(): HTMLElement
	{
		if( typeof this.#thumbnail !== "undefined" ) return this.#thumbnail;

		const html =
		`
			<figure class="thumb">
				<picture tabindex="0"></picture>
				<figcaption>
					<span class="title">${ this.title }</span>
					<span class="likes">${ this.likes }

					<div class="heart" tabindex="0" role="checkbox" aria-checked="false">
						<img src="img/icons/heart-solid.svg" alt="Liker l'image">
					</div>
				</figcaption>
			</figure>
		`;

		this.#thumbnail = util.toDom( html );
		const picture   = this.#thumbnail.querySelector( "picture" )!;
		const heart     = this.#thumbnail.querySelector( ".heart"  )!;

		picture.append( this.element() );

		picture.addEventListener( "click", this.#onOpenCarousel.bind(this       ) );
		heart  .addEventListener( "click", this.#onLikesClicked.bind(this, heart) );

		return this.#thumbnail;
	}



	#onOpenCarousel() : void
	{
		const event = new CustomEvent( "open-carousel", { detail: this } );
		document.body.dispatchEvent( event );
	}



	// Event handler for clicks on the heart. We keep track of wheter we were liked
	// already in which case we decrement.
	//
	#onLikesClicked( heart: Element ) : void
	{
		const likes  = this.#thumbnail!.querySelector( ".likes" )!;
		const num    = likes.firstChild!;
		let   parsed = parseInt( num.textContent! );


		if( this.#liked )
		{
			--parsed;
			heart.setAttribute( "aria-checked", "false" );
		}

		else
		{
			++parsed;
			heart.setAttribute( "aria-checked", "true" );
		}



		this.#liked = !this.#liked;

		num.textContent = parsed.toString();
		console.assert( parsed >= 0 );


		const event = this.#liked ?

			  new CustomEvent( "likes-increment" )
			: new CustomEvent( "likes-decrement" )
		;


		document.body.dispatchEvent( event );
	}

}



class Image extends Media
{
	url  : string                      ;
	#elem: HTMLImageElement | undefined;



	constructor( json: Iface.Media )
	{
		super(json);

		this.url = json.image!;
	}



	override element() : HTMLElement
	{
		if( typeof this.#elem !== "undefined" ) return this.#elem;

		const html = `<img class="media-content" src="img/${this.photographerID}/${this.url}" alt="${this.title}, closeup view">`;

		this.#elem = util.toDom( html ) as HTMLImageElement;

		return this.#elem!;
	}



	override carousel() : HTMLElement
	{
		const img = this.element().cloneNode(true) as HTMLImageElement;

		// Remove ", closeup view"
		//
		img.setAttribute( "alt", this.title );

		return img;
	}



	override naturalHeight() : number
	{
		const img = this.element() as HTMLImageElement;

		return img.naturalHeight;
	}



	override naturalWidth() : number
	{
		const img = this.element() as HTMLImageElement;

		return img.naturalWidth;
	}
}




class Video extends Media
{
	url    : string                       ;
	#elem  : HTMLVideoElement | undefined ;

	// we buffer this because on back button popstate on video, the video isn't always loaded when
	// we need to know it's size, in which case it will return 0.
	//
	#width : number | undefined ;
	#height: number | undefined ;



	constructor( json: Iface.Media )
	{
		super(json);

		this.url = json.video!;
	}



	override element() : HTMLElement
	{
		if( typeof this.#elem !== "undefined" ) return this.#elem;

		const html =
		`
			<video class="media-content" tabindex="-1">
				<source src="img/${this.photographerID}/${this.url}" type="video/mp4">
				Your browser does not support the video tag.
			</video>
		`;

		this.#elem = util.toDom( html ) as HTMLVideoElement;

		return this.#elem!;
	}



	override carousel() : HTMLElement
	{
		const video = this.element().cloneNode(true) as HTMLVideoElement;

		video.setAttribute( "controls", ""  );
		video.setAttribute( "tabindex", "0" );

		return video;
	}



	override naturalHeight() : number
	{
		if( typeof this.#height !== "undefined" ) return this.#height;

		const vid = this.element() as HTMLVideoElement;
		this.#height = vid.videoHeight;

		return this.#height;
	}



	override naturalWidth() : number
	{
		if( typeof this.#width !== "undefined" ) return this.#width;

		const vid = this.element() as HTMLVideoElement;
		this.#width = vid.videoWidth;

		return this.#width;
	}
}
