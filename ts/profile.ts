import { FishEyeData  } from "./fishEyeData.js";
import { Media        } from "./media.js";
import { Photographer } from "./photographer.js";
import { Gallery      } from "./gallery.js";
import { Contact      } from "./contact.js";
import { Router       } from "./router.js";



export class Profile
{
	// The media of this photographer.
	//
	#media       : Media[];
	#photographer: Photographer;
	#gallery     : Gallery;


	constructor( data: FishEyeData, router: Router )
	{
		this.#photographer = this.#getPhotographer( data, router );
		this.#media        = data.mediaByAuthor( this.#photographer );

		const main = document.querySelector( "main" ) as HTMLElement;
		main.prepend( this.#photographer.profileHeading() );

		this.#gallery = new Gallery( this.#media );
		new Contact( this.#photographer.name );     // functionality of contact form.

		this.#likesPrice();

		document.title = `FishEye profile: ${this.#photographer.name}`;
	}


	// Fill data in the bottom right component which shows total likes and price.
	//
	#likesPrice() : void
	{
		const price = document.querySelector( "#likes-price > .price" ) as HTMLElement;
		const likes = document.querySelector( "#likes-price > .likes" ) as HTMLElement;

		price.innerText = `${ this.#photographer.price }€/jour`;
		likes.prepend( this.#gallery.totalLikes.toString() );
	}


	#getPhotographer( data: FishEyeData, router: Router ) : Photographer
	{
		const id = router.getQueryVar( "id" );

			// TODO: redirect to main page?
			//
			if( id === null ) throw "Profile: Cannot load profile without ID.";

		const parsed = parseInt( id, 10 );

			if( isNaN(parsed) ) throw "Profile: ID must be a number.";

		const p = data.findPhotographer( parsed );

			if( typeof p === "undefined" ) throw "Profile: Unknown photographer.";
			else                           return p;
	}
}
