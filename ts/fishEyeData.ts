import { Media        } from "./media.js";
import { Photographer } from "./photographer.js";
import * as Iface       from "./deser.js";


export class FishEyeData
{
	photographers: Photographer[];
	media        : Media[];


	constructor( json: Iface.FishEyeData )
	{
		this.photographers = [];
		this.media         = [];

		for( const p of json.photographers )
		{
			this.photographers.push( new Photographer(p) );
		}

		for( const m of json.media )
		{
			this.media.push( Media.createMedia(m) );
		}

		Object.freeze( this );
	}


	allTags(): Set<string>
	{
		let all: string[] = [];

		for( const p of this.photographers )
		{
			all = all.concat( p.tags );
		}


		return new Set( all.sort() );
	}


	findPhotographer( id: number ) : Photographer | undefined
	{
		return this.photographers.find( p => p.id == id );
	}


	mediaByAuthor( p: Photographer ) : Media[]
	{
		return this.media.filter( m => m.photographerID == p.id );
	}
}
