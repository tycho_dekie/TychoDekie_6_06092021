// Represents the tag cloud and fires events when tags are clicked.
//
export class TagCloud
{
	#tags    : Map<string, boolean> ;
	container: HTMLDivElement       ;



	constructor( tags: string[] )
	{
		this.#tags = new Map;

		for( const t of tags )
		{
			this.#tags.set( t, false );
		}

		this.container = document.createElement( "div" );
		this.container.classList.add( "tags" );

		// https://github.com/microsoft/TypeScript/issues/28357#issuecomment-711131035
		//
		window.addEventListener( "filter-change", { handleEvent: this.#onFilter.bind(this) } );
	}



	html( label: string ) : HTMLDivElement
	{
		for( const s of this.#tags )
		{
			const t = document.createElement( "span" );

			t.setAttribute( "aria-label"  , `${label}${s[0]}` );
			t.setAttribute( "tabindex"    , "0"               );
			t.setAttribute( "role"        , "checkbox"        );
			t.setAttribute( "aria-checked", "false"           );

			t.classList.add( "tag" );
			t.classList.add( "tag--" + s[0] );
			t.innerText = s[0];

			t.addEventListener( "click", this.toggle.bind(this, s[0]) );

			this.container.appendChild( t );
		}

		return this.container;
	}



	toggle( tag: string ) : void
	{
		if( !this.#tags.has(tag) ) throw "TagCloud: Unknown tag clicked";

		this.#tags.get(tag) ?

			  this.#tags.set( tag, false )
			: this.#tags.set( tag, true  )
		;

		const event = new CustomEvent( "filter-change", { detail: new Map(this.#tags) } );
		window.dispatchEvent( event );
	}



	// We need to make sure that different tag clouds on the page stay in sync.
	//
	#onFilter( evt: CustomEvent< Map<string, boolean> > ) : void
	{
		this.#tags = evt.detail;
		this.#updateDom();
	}



	#updateDom() : void
	{
		for( const [tag, checked] of this.#tags )
		{
			const span = this.container.querySelector( ".tag--" + tag );

			// The tagclouds on the photographers don't have all the tags.
			//
			if( !span ) continue;

			if( checked )
			{
				span.classList.add( "filter__active"      );
				span.setAttribute( "aria-checked", "true" );
			}

			else
			{
				span.classList.remove( "filter__active"    );
				span.setAttribute( "aria-checked", "false" );
			}
		}
	}
}
