import * as Iface   from "./deser.js";
import * as util    from "./util.js";
import { TagCloud } from "./tagCloud.js";

export class Photographer
{
	name     : string                       ;
	id       : number                       ;
	city     : string                       ;
	country  : string                       ;
	tags     : string[]                     ;
	tagline  : string                       ;
	price    : number                       ;
	portrait : string                       ;
	#summary : HTMLDivElement | null = null ;


	constructor( json: Iface.Photographer )
	{
		this.name     = json.name     ;
		this.id       = json.id       ;
		this.city     = json.city     ;
		this.country  = json.country  ;
		this.tags     = json.tags     ;
		this.tagline  = json.tagline  ;
		this.price    = json.price    ;
		this.portrait = json.portrait ;

		Object.freeze( this.tags );
	}


	// Create the html for the profile summary shown on the home page.
	// It will be stored for re-use.
	//
	profileSummary() : HTMLDivElement
	{
		if( this.#summary !== null ) return this.#summary;

		const html =
		`
		<div class="profile-summary profile-${this.id}">

			<a href="profile.html?id=${this.id}" tabindex="-1">
				<div class="portrait">
					<img class="portrait--img" src="img/id/${this.portrait}" alt="Portrait du/de la photographe ${this.name}">
					<div class="portrait--shadow"></div>
				</div>
				<h2 class="name"><a href="profile.html?id=${this.id}" aria-label="Liens vers le profile de ${this.name}">${this.name}</a></h2>
			</a>

			<p class="location">${this.city}, ${this.country}</p>
			<p class="tagline">${this.tagline}</p>
			<p class="price">${this.price}€/jour</p>

		</div>

		`;

		this.#summary = util.toDom( html ) as HTMLDivElement;
		const cloud   = new TagCloud( this.tags );

		this.#summary.appendChild( cloud.html( "Filtrer les images par le mot clef: " ) );

		return this.#summary;
	}


	// Generate the block on top of the profile page.
	//
	profileHeading() : HTMLDivElement
	{
		const html =
		`
		<div class="profile-heading">

			<div class="profile--align">

				<div class="profile--text">
					<div class="name--container">
						<h1 class="name">${this.name}</h1>
						<button id="button--contact">Contactez-moi</button>
					</div>
					<p class="location">${this.city}, ${this.country}</p>
					<p class="tagline">${this.tagline}</p>
				</div>

				<div class="portrait">
					<img class="portrait--img" src="img/id/${this.portrait}" alt="">
					<div class="portrait--shadow"></div>
				</div>

			</div>

			${this.tagList()}

		</div>

		`;

		const div = util.toDom( html ) as HTMLDivElement;

		return div;
	}


	tagList() : string
	{
		let tags = "";

		for( const t of this.tags )
		{
			tags += `<a href="index.html?filter=${t}" class="tag" aria-label="Filtrer les photographes par le mot clé: ${t}">${t}</a>`;
		}

		return `<div class="tags">${tags}</div>`
	}
}
