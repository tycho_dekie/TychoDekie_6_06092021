import { FishEyeData } from "./fishEyeData.js";
import { Router      } from "./router.js";
import { Home        } from "./home.js";
import { Profile     } from "./profile.js";

import * as Iface from "./deser.js";


main().then();


async function main()
{
	const JsonUrl = "FishEyeData.json";

	const resp = await fetch( JsonUrl );
	const json = await resp.text();


	// Allow keyboard presses to be considered clicks. This allows all code that
	// has onclick activation to just work for keyboard.
	//
	window.addEventListener( "keypress", keyPressed );


	const fishEyeData = Iface.Convert.toFishEyeData( json );

	const data   = new FishEyeData( fishEyeData );
	const router = new Router;


	switch( router.page )
	{
		case "index"  : new Home   ( data, router )  ; break;
		case "profile": new Profile( data, router )  ; break;
		default       : throw( "Unreachable branch" );
	}
}


function keyPressed( evt: KeyboardEvent ) : void
{
	if
	(
		   document.activeElement instanceof HTMLElement
		&& evt.key === "Enter"
		&& !(evt.target instanceof HTMLButtonElement) // We would get a double click.
	)
	{
		document.activeElement.click();
	}
}
