import { Media    } from "./media.js";
import { DropDown } from "./dropDown.js";

type Order = "Popularité" | "Titre" | "Date";

// Takes care of everything dynamic on the gallery and carousel that is on the photographer
// profile page.
//
// Saves history state so navigating with the back button becomes possible between images in the
// carousel as well as when changing the filters or the ordering.
//
export class Gallery
{
	#carousel  : HTMLElement ;
	#close     : HTMLElement ;
	#figure    : HTMLElement ;
	#gallery   : HTMLElement ;
	#media     : Media[]     ;
	#next      : HTMLElement ;
	#prev      : HTMLElement ;
	#state     : GalleryState; // index in array, the current image in the carousel.
	totalLikes : number      ;
	#trigger   : HTMLElement ; // The thumbnail that triggered the carousel on click. Used to give it focus on carousel close.

	// Store the event listener for keyboard events. As we bind it, we need to store
	// it so we can remove the event listener again.
	//
	#onKeyDown : ( this: Document, e: KeyboardEvent ) => void;


	// @param media The list of media to display in this gallery.
	//
	constructor( media: Media[] )
	{
		this.#prev     = document      .querySelector( ".carousel .prev"          )!;
		this.#next     = document      .querySelector( ".carousel .next"          )!;
		this.#close    = document      .querySelector( ".carousel .close"         )!;
		this.#gallery  = document      .querySelector( ".gallery--content"        )!;
		this.#carousel = document      .querySelector( ".carousel"                )!;
		this.#figure   = this.#carousel.querySelector( "figure"                   )!;

		this.#prev .addEventListener( "click"   , this.#movePrev       .bind(this) );
		this.#next .addEventListener( "click"   , this.#moveNext       .bind(this) );
		this.#close.addEventListener( "click"   , this.#onCloseCarousel.bind(this) );
		window     .addEventListener( "resize"  , this.#imgResize      .bind(this) );
		window     .addEventListener( "popstate", this.#onPopState     .bind(this) );

		this.#onKeyDown = this.#keyboardInput.bind( this );

		const dropContainer = document.querySelector( "#sort" ) as HTMLDivElement;
		const entries       = [ "Popularité", "Date", "Titre" ];
		new DropDown( dropContainer, "Trier par", entries, this.#onSort.bind(this) );


		this.#media    = media;
		const filtered = [ ... Array( media.length ).keys() ];
		this.#state    = new GalleryState( null, filtered, "Popularité" );
		this.#sort();

		// We don't want to create a new state right after loading the page, but we want to save this
		// because popsate will be invoked when the back button is pressed.
		//
		this.#state.replace();


		function sum( accu: number, m: Media ): number
		{
			return accu + m.likes;
		}

		this.totalLikes = this.#media.reduce( sum, 0 );


		document.body.addEventListener( "open-carousel"  , { handleEvent: this.#onOpenCarousel.bind(this) } );
		document.body.addEventListener( "likes-increment", { handleEvent: this.#onLikesInc    .bind(this) } );
		document.body.addEventListener( "likes-decrement", { handleEvent: this.#onLikesDec    .bind(this) } );
	}



	#getMedia( mIdx: number ) : Media
	{
		console.assert( mIdx <= this.#media.length, mIdx, this.#media.length );

		return this.#media[mIdx]!;
	}



	// Produce the html for the gallery component.
	//
	#render() : void
	{
		// Be idempotent.
		//
		this.#gallery.innerHTML = "";


		for( const mIdx of this.#state.filtered )
		{
			const m     = this.#getMedia( mIdx );
			const thumb = m.thumbnail();

			this.#gallery.append( thumb );
		}
	}



	#onLikesInc() : void
	{
		const totalLikes = document.querySelector( "#likes-price > .likes" ) as HTMLElement;

		totalLikes.innerText = (++this.totalLikes).toString();
	}



	#onLikesDec() : void
	{
		const totalLikes = document.querySelector( "#likes-price > .likes" ) as HTMLElement;

		totalLikes.innerText = (--this.totalLikes).toString();
	}



	// Event listener on each media in the gallery. Opens the carousel.
	//
	#onOpenCarousel( evt: CustomEvent<Media> ) : void
	{
		const mIdx = this.#media         .indexOf( evt.detail );
		const fIdx = this.#state.filtered.indexOf( mIdx       );

		if( fIdx === -1 ) throw "Gallery: Trying to open carousel on an image which is not currently in the gallery.";

		this.#state.carousel = fIdx;
		this.#trigger = evt.detail.thumbnail().querySelector( "picture" )!;
		this.#state.push();

		this.#openCarousel();
		this.#next.focus();
	}


	#openCarousel() : void
	{
		document.body.classList.add( "body__carousel" );
		document.addEventListener( "keydown", this.#onKeyDown );

		this.#updateCarousel();
	}



	// Event listener on close button of carousel.
	//
	#onCloseCarousel() : void
	{
		this.#closeCarousel();

		this.#state.carousel = null;
		this.#state.push();
	}


	#closeCarousel() : void
	{
		document.removeEventListener( "keydown", this.#onKeyDown );

		document.body.classList.remove( "body__carousel" );

		this.#trigger.focus();
	}



	// Event listener for previous button on carousel.
	//
	#movePrev() : void
	{
		// make sure the state is valid (open carousel).
		//
		if( this.#state.carousel         === null ) return;
		if( this.#state.filtered.length  <   1    ) throw "Gallery.#movePrev: Empty gallery?";

		this.#state.prev();

		this.#updateCarousel();

		this.#carousel.setAttribute( "role", "alert" );
	}



	// Event listener for next button on carousel.
	//
	#moveNext() : void
	{
		// make sure the state is valid (open carousel).
		//
		if( this.#state.carousel === null ) return;

		this.#state.next();

		this.#updateCarousel();

		this.#carousel.setAttribute( "role", "alert" );
	}



	// Update the carousel to respect the value of "this.#state.carousel" for which media to show.
	// Callers need to guarantee that this.#state.carousel is not null and a valid index in the array.
	//
	#updateCarousel() : void
	{
		if( this.#state.carousel! < 0                            ) throw "negative index in gallery: "           + this.#state.carousel;
		if( this.#state.carousel! >= this.#state.filtered.length ) throw "index in gallery bigger than length: " + this.#state.carousel;

		const mIdx = this.#state.filtered[ this.#state.carousel! ]!;
		const m    = this.#media[ mIdx ]!;

		const content = this.#figure.querySelector( ".media-content" )!;
		const caption = this.#figure.querySelector( "figcaption"     )!;

		content && content.remove();
		this.#figure.prepend( m.carousel() );

		caption.innerText = m.title;
		this.#carousel.setAttribute( "aria-label", `Image (${m.title}), closeup view` );

		this.#imgResize();
	}



	// Called from #updateCarousel() and from event window.resize to recalculate the size of the image in the carousel.
	// We compare the aspect ratio with that of the viewport and constrain the image accordingly.
	//
	// Adjustments are made for the buttons and the caption as well as a little margin around the whole carousel. The
	// calculated size is set on the actual image and not the carousel, as we can't really calculate the proper aspect
	// ratio of the image+buttons+caption before we know the image size, so that doesn't work. However we can se the image itself
	// and the carousel around it will adopt to the size of the content.
	//
	#imgResize() : void
	{
		// carousel is not open.
		//
		if( this.#state.carousel === null ) return;

		const mIdx = this.#state.filtered[ this.#state.carousel! ]!;
		const m    = this.#media[ mIdx ]!;

		// Calculate the aspect ratio of the carousel:
		//
		const buttonWidth   = this.#prev.offsetWidth *2;
		const captionHeight = this.#figure.querySelector( "figcaption" )!.offsetHeight;
		const mAspect       = m.naturalWidth() / m.naturalHeight();
		const viewport      = this.#windowSize( buttonWidth, captionHeight );
		const hMargin       = viewport.h / 20; // 5% of height
		const wMargin       = viewport.w / 20; // 5% of width

		let width : number;

		// Image is wider than viewport
		//
		if( mAspect > viewport.aspect )
		{
			width  = viewport.w - buttonWidth - wMargin;
		}

		// image is higher than viewport
		// We only substract a margin here, as the buttons on the side are properly spaced already.
		// However if we use the full height here, our image will touch the top of the screen and our
		// caption the bottom.
		//
		else
		{
			const height = viewport.h - captionHeight - hMargin;
			      width  = height * mAspect;
		}


		this.#carousel.style.setProperty( "--carousel-width" , Math.floor(width) + "px" );
	}



	#windowSize( buttonWidth: number, captionHeight: number ) : { h: number, w: number, aspect: number }
	{
		const h = document.documentElement.clientHeight;
		const w = document.documentElement.clientWidth ;

		// Take into account our button and caption sizes.
		//
		const aspect = ( w - buttonWidth ) / ( h - captionHeight );

		return { h, w, aspect };
	}



	// Handles keyboard input in the carousel.
	// It's attached to keyup, only when the carousel is open.
	//
	#keyboardInput( evt: KeyboardEvent ) : void
	{
		switch( evt.key )
		{
			case "ArrowLeft" : this.#movePrev()     ; break;
			case "ArrowRight": this.#moveNext()     ; break;
			case "Escape"    : this.#closeCarousel(); break;
		}
	}



	#onPopState( evt: PopStateEvent ) : void
	{
		if( evt.state === null || typeof evt.state.gallery === "undefined" ) return;

		const state = evt.state.gallery;

		// The browser serializes the state object and does not respect class on deserialization. We need to
		// construct it again.
		//
		this.#state = new GalleryState( state.carousel, state.filtered, state.order );


		// Has to be before we render the carousel, as width and height on the image will
		// return 0 if it's not in the dom.
		//
		this.#render();


		this.#state.carousel === null ?

			  this.#closeCarousel()
			: this.#openCarousel()
		;
	}



	// Event listener.
	//
	#onSort( val: string ) : void
	{
		if( val === this.#state.order ) return;

		this.#state.order = val as Order;
		this.#sort();

		// The dropdown will do the push first.
		//
		this.#state.replace();
	}



	#sort() : void
	{
		switch( this.#state.order )
		{
			case "Popularité": this.#state.filtered.sort( this.#sortPopular.bind(this) ); break;
			case "Titre"     : this.#state.filtered.sort( this.#sortTitle  .bind(this) ); break;
			case "Date"      : this.#state.filtered.sort( this.#sortDate   .bind(this) ); break;

			default: throw "Unknown sort type.";
		}

		this.#render();
	}



	// @return positive number is sort two before one.
	//
	#sortPopular( mIdx1: number, mIdx2: number ) : number
	{
		const likes1 = this.#media[ mIdx1 ]!.likes;
		const likes2 = this.#media[ mIdx2 ]!.likes;

		return likes2 - likes1;
	}



	// @return positive number is sort two before one.
	//
	#sortTitle( mIdx1: number, mIdx2: number ) : number
	{
		const title1 = this.#media[ mIdx1 ]!.title;
		const title2 = this.#media[ mIdx2 ]!.title;

		if( title2 < title1 ) return  1;
		if( title2 > title1 ) return -1;

		// They are identical.
		//
		return 0;
	}



	// @return positive number is sort two before one.
	//
	#sortDate( mIdx1: number, mIdx2: number ) : number
	{
		const date1 = this.#media[ mIdx1 ]!.date;
		const date2 = this.#media[ mIdx2 ]!.date;

		if( date2 < date1 ) return  1;
		if( date2 > date1 ) return -1;

		// They are identical.
		//
		return 0;
	}
}



// Keeps track of whether the carousel is open, which image it's on and how the gallery is sorted.
// Modification of this state happens through setters, which will take care of calling window.history.pushState.
// This allows the back button to work when browsing through the carousel.
//
class GalleryState
{
	// The state of the carousel. null = closed, otherwise the index of the media to be shown.
	// Cannot be private, because window.history.pushState doesn't serialize private properties.
	//
	carousel: number | null ;
	filtered: number[]      ;
	order   : Order;


	constructor( carousel: number | null, filtered: number[], order: Order )
	{
		this.carousel = carousel ;
		this.filtered = filtered ;
		this.order    = order    ;
	}


	// Decrement the index, looping round to the end. The caller has to guarantee that
	// carousel is not null.
	// @param max the highest valid index in the media array.
	// @return the new value.
	//
	prev() : number
	{
		if( this.carousel === null ) throw "GalleryState: trying to substract from null carousel idx.";

		let previous = this.carousel - 1;

		if( previous < 0 ) previous = this.filtered.length - 1;

		this.carousel = previous;
		this.push();

		return this.carousel;
	}


	// Increment the index, looping round to the beginning. The caller has to guarantee that
	// carousel is not null.
	// @param length the length of the media array.
	// @return the new value.
	//
	next() : number
	{
		if( this.carousel === null ) throw "GalleryState: trying to add to null carousel idx.";

		// Just loop round to the start.
		// Note that we do increment on _carousel to avoid triggering pushstate, as increment is an assignment.
		//
		this.carousel = ++this.carousel % this.filtered.length;
		this.push();

		return this.carousel;
	}


	push() : GalleryState
	{
		const state = window.history.state || {};
		state.gallery = this;

		window.history.pushState( state, document.title, "" );
		return this;
	}


	replace() : GalleryState
	{
		const state = window.history.state || {};
		state.gallery = this;

		window.history.replaceState( state, document.title, "" );
		return this;
	}
}
