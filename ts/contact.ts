
// Manages the contact form on the profile page. We capture focus when it's open.
//
export class Contact
{
	#firstFocusableElement : HTMLElement
	#focusableContent      : NodeListOf<Element>;
	#focusableElements     : string;
	#form                  : HTMLFormElement;
	#lastFocusableElement  : HTMLElement
	#onKeyDown             = this.#loopFocus.bind(this);

	constructor( name: string )
	{
		this.#form                  = document.getElementById( "contact--form" ) as HTMLFormElement;
		this.#focusableElements     = "button, [href], input, select, textarea, [tabindex]:not([tabindex=\"-1\"])";
		this.#focusableContent      = this.#form.querySelectorAll( this.#focusableElements );
		this.#firstFocusableElement = this.#focusableContent[0]!                                   as HTMLElement;
		this.#lastFocusableElement  = this.#focusableContent[ this.#focusableContent.length - 1 ]! as HTMLElement;

		const openButton  = document.getElementById( "button--contact" )!;
		const closeButton = document.getElementById( "contact--close"  )!;
		const title       = document.getElementById( "contact--name"   )!;

		title.innerText = name;

		openButton .addEventListener( "click" , this.openDialog .bind(this) );
		closeButton.addEventListener( "click" , this.closeDialog.bind(this) );
		this.#form .addEventListener( "submit", this.submit     .bind(this) );
	}



	openDialog(): void
	{
		document.body.classList.add( "body__contact" );

		document.addEventListener( "keydown", this.#onKeyDown );

		document.getElementById( "prenom" )!.focus();
	}



	closeDialog(): void
	{
		document.body.classList.remove( "body__contact" );

		document.removeEventListener( "keydown", this.#onKeyDown );

		document.getElementById( "button--contact" )!.focus();
	}



	submit( evt: Event ): void
	{
		evt.preventDefault();

		const  prenom = this.#form.elements.namedItem( "prenom"  ) as HTMLInputElement;
		const     nom = this.#form.elements.namedItem( "nom"     ) as HTMLInputElement;
		const   email = this.#form.elements.namedItem( "email"   ) as HTMLInputElement;
		const message = this.#form.elements.namedItem( "message" ) as HTMLTextAreaElement;

		console.log( "form submitted with:" );
		console.log( " prenom:",  prenom.value );
		console.log( "    nom:",     nom.value );
		console.log( "  email:",   email.value );
		console.log( "message:", message.value );

		this.closeDialog();
	}



	// Make sure focus stays within the modal while it is open. We make sure it loops over on first and last elements.
	// Inspired from: https://uxdesign.cc/how-to-trap-focus-inside-modal-to-make-it-ada-compliant-6a50f9a70700
	//
	#loopFocus( evt: KeyboardEvent ) : void
	{
		if( evt.key === "Escape" )
		{
			this.closeDialog();
			return;
		}


		if( evt.key !== "Tab" ) return;

		// if shift key pressed for shift + tab combination on the first element.
		//
		if( document.activeElement === this.#firstFocusableElement  &&  evt.shiftKey )
		{
			this.#lastFocusableElement.focus();
			evt.preventDefault();
		}

		else if( document.activeElement === this.#lastFocusableElement  &&  !evt.shiftKey )
		{
			this.#firstFocusableElement.focus();
			evt.preventDefault();
		}
	}
}
